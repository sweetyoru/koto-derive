#![allow(non_snake_case)]			// turn off warning about snake_case name
#![allow(non_camel_case_types)]		// turn off warning about upper camel case type name
#![allow(non_upper_case_globals)]	// turn off warning about global non upper camel name


use proc_macro::TokenStream;

use syn;
use quote::quote;


#[proc_macro_derive(KotoTrait, attributes(KotoTraitSkip))]
pub fn KotoDerive (input: TokenStream) -> TokenStream
{
	// Construct a representation of Rust code as a syntax tree
	// that we can manipulate
	let astTree = syn::parse (input).unwrap ();

	// Build the trait implementation
	ImplKotoTrait (&astTree)
}


fn ImplKotoTrait (astTree: &syn::DeriveInput) -> TokenStream
{
	let name = &astTree.ident;
	// let generics = &astTree.generics;
	// let whereClause = &astTree.generics.where_clause;
	// let fields = &astTree.data;
	let (intro_generics, fwd_generics, mb_where_clause) = astTree.generics.split_for_impl();

	let gen = quote!
	{
		impl #intro_generics KotoTrait for #name #fwd_generics #mb_where_clause
		// impl #generics KotoTrait for #name #generics #whereClause
		{
			fn OutputBytesSize (&self) -> usize
			{
				// size
				// element count
				// elements
				unimplemented! ();
			}

			fn FromBytes (data: &[u8]) -> Result<(Self, usize), KotoError>
			{
				unimplemented! ();
			}

			fn ToBytesInplace (&self, output: &mut [u8]) -> Result<usize, KotoError>
			{
				unimplemented! ();
			}
		}
	};

	gen.into ()

	// now mess with data
}